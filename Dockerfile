FROM parity/parity:stable

# install all dependencies
USER root
RUN apt-get update \
	&& apt-get install --yes --no-install-recommends curl \
	&& rm -rf /var/lib/apt/lists/*
WORKDIR /
COPY dev-key.json /parity/keys/AuraChain/dev-key.json
COPY aura-chain-spec.json /parity/aura-chain-spec.json
COPY aura-config.toml /parity/aura-config.toml
COPY ./common_start.sh /common_start.sh
COPY start.sh /start.sh
RUN echo "" > /parity/password
RUN chmod +x /start.sh

# USER parity
ENTRYPOINT [ "/start.sh" ]

# docker image build --tag parity-poa .
# docker container run --rm -p 8000:8000 -p 8001:8001 -p 8545:8545 -p 8546:8546 -d --name parity-poa parity-poa
